Vue.component('food-row', {
    props: ['food'],
    template:
        '<div>' +
            '<tr align="center">' +
                '<td height="40" width="350px">{{ food.name }}</td>' +
                '<td width="150px">{{ food.mustBuy }}</td>' +
            '</tr>' +
        '</div>'
});

Vue.component('foods-list', {
    props: ['foods'],

    template:
        '<div align="center">' +
            '<h1 class="mt-3">Требуется</h1>' +
            '<table-header/>' +
            '<table class="table-bordered table-light " cellpadding="7" >' +
                '<food-row v-for="food in foods" :key="food.id" :food="food"  />' +
            '</table>' +
        '</div>'
});

Vue.component('table-header', {
    template:
        '<table class="table-bordered table-dark mt-3">' +
            '<tr align="center">' +
                '<th scope="col" width="350px" height="55">Название продукта</th>' +
                '<th scope="col" width="150px">Нужно закупить</th>' +
            '</tr>' +
        '</table>'
});


var app = new Vue({
    el: '#app',
    template: '<foods-list :foods="foods" />',
    data: {
        foods: []
    },
    methods: {
        getFoods: function () {
            this.$http.get('/mustbuylist').then(function (response){
                this.foods = response.data
            }, function (error) {
            })
        }
    },
    created: function () {
        this.getFoods()
    }
});