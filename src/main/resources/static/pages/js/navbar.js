
Vue.component('navbar', {
    props: ['activeUser'],

    template:
    '<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">'+
        '<a class="navbar-brand" href="/pages/main.html">RestaurantSupport</a>'+
        '<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">'+
            '<span class="navbar-toggler-icon"></span>'+
        '</button>'+

        '<div class="collapse navbar-collapse" id="navbarSupportedContent">'+
            '<ul class="navbar-nav mr-auto">'+
                '<li class="nav-item dropdown">'+
                    '<a class="nav-link dropdown-toggle"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        'Таблицы'+
                    '</a>'+
                    '<div class="dropdown-menu" aria-labelledby="navbarDropdown">'+
                        '<a class="dropdown-item" href="kitchentable.html">Для заказов с кухни</a>'+
                        '<a class="dropdown-item" href="stocktable.html">Для работы на складе</a>'+
                        '<a class="dropdown-item" href="mustbuytable.html">Для закупщика</a>'+
                        '<div class="dropdown-divider"></div>'+
                        '<a class="dropdown-item" href="help.html">Помощь</a>'+
                    '</div>'+
                '</li>'+
                '<li v-if="activeUser.roles.indexOf(\'ADMIN\') !== -1 ">'+
                    '<a  class="nav-link" href="users.html">Пользователи</a>'+
                '</li>'+
            '</ul>'+
            '<span class="navbar-text">'+
                '{{ activeUser.username }}' +
            '</span>'+
            '<form class="nav-link"  action="/logout" method="post">'+
                '<button class="btn btn-secondary" type="submit">Log Out</button>'+
            '</form>'+
        '</div>'+
    '</nav>',
});


var app = new Vue({
    el: '#nav',
    template: '<navbar :activeUser="activeUser"/>',
    data: {
        activeUser: {}
    },
    methods: {
        getActiveUser: function () {
            this.$http.get('/activeuser').then(function (response){
                this.activeUser = response.data
            }, function (error) {
            })
        }
    },
    created: function () {
        this.getActiveUser()
    }
});