function getIndex(list, id) {
    for (var i = 0; i < list.length; i++ ) {
        if (list[i].id === id) {
            return i;
        }
    }

    return -1;
}


var userApi = Vue.resource('/users');

Vue.component('user-form', {
    props: ['users', 'userAttr', 'message', 'setMessage'],
    data: function() {
        return {
            originpass: '',
            username: '',
            id: '',
            role:''
        }
    },
    template:
        '<div>' +
            '<div v-if="message.code === true" class="alert alert-success" >' +
                '{{message.message}}' +
            '</div>' +
            '<div v-if="message.code === false" class="alert alert-danger" >' +
                '{{message.message}}' +
            '</div>' +
            '<div class="form-inline col-md-6 mt-3 mb-3 ml-5" align="center">' +
            '<input class="form-control" type="text" placeholder="Имя" v-model="username">' +
            '<input class="form-control ml-1" type="text" placeholder="Пароль" v-model="originpass">' +
            '<input class="btn btn-primary ml-2" type="button" value="Save" @click="save" />' +
            '<p class="mr-3"><input name="role" type="radio" v-model="role" value="ADMIN">ADMIN</p>' +
            '<p class="mr-3"><input name="role" type="radio" v-model="role" value="STOCK">STOCK</p>'+
            '<p class="mr-3"><input name="role" type="radio" v-model="role" value="KITCHEN">KITCHEN</p>'+
            '<p class="mr-3"><input name="role" type="radio" v-model="role" value="AGENT">AGENT</p>'+
            '</div>' +
        '</div>',
    methods: {
        save: function() {
            var user = { username: this.username, originpass: this.originpass, role: this.role};

            userApi.save({}, user).then(result =>
                result.json().then(data => {
                    if(data.message.code === true){ this.users.push(data.body);}
                    this.setMessage(data.message);
                    this.originpass = '';
                    this.username = '';
                })
            )

        }
    }
});

Vue.component('user-row', {
    props: ['user', 'users', 'setMessage'],
    template:
        '<div>' +
        '<tr align="center">' +
        '<td width="350px" height="40">{{ user.username }}</td>' +
        '<td width="150px">{{ user.originpass }}</td>' +
        '<td width="150px">{{ user.role }}</td>' +
        '<td width="200px">' +
        '<span align="center">' +
        '<input class="btn btn-primary ml-1 " type="button" value="X" @click="del" />' +
        '</span>' +
        '</td>' +
        '</tr>' +
        '</div>',
    methods: {
        del: function() {
            userApi.delete({},this.user).then(result =>
                result.json().then(data => {
                    if(data.message.code === true){
                        this.users.splice(this.users.indexOf(this.user), 1)
                    }
                    this.setMessage(data.message)
                })
            )
        }
    }
});

Vue.component('users-list', {
    props: ['users', 'message', 'setMessage'],
    data: function() {
        return {
            user: null
        }
    },
    template:
        '<div align="center">' +
        '<h1 class="mt-3">Пользователи</h1>' +
        '<user-form :setMessage="setMessage" :message="message" :users="users" :foodAttr="user" />' +
        '<table-header  />' +
        '<table class="table-bordered table-light" cellpadding="7" >' +
        '<user-row v-for="user in users" :setMessage="setMessage" :key="user.id" :user="user" :users="users" />' +
        '</table>' +
        '</div>'
});

Vue.component('table-header', {
    template:
        '<div>' +
        '<table class="table-bordered table-dark mt-2 " cellpadding="10" >' +
        '   <tr align="center">' +
        '<th scope="col" width="350px" height="55">Имя</th>' +
        '<th scope="col" width="150px">Пароль</th>' +
        '<th scope="col" width="150px">Роль</th>' +
        '<th scope="col" width="200px">Изменить</th>' +
        '</tr>' +
        '</table>' +
        '</div>'
});



var app = new Vue({
    el: '#app',
    template: '<users-list :setMessage="setMessage" :message="message" :users="users" />',
    data: {
        users: [],
        message:{message:" "}
    },
    methods: {
        getUsers: function () {
            this.$http.get('/users').then(function (response){
                this.users = response.data
            }, function (error) {
            })
        },
        setMessage: function (message) {
            this.message = message;
        },
        getMessage: function () {
          this.message = {code:"", message: ""}
        }
    },
    created: function() {
        this.getUsers();
        this.setMessage();
        this.getMessage();
    }
});