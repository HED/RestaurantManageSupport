function getIndex(list, id) {
    for (var i = 0; i < list.length; i++ ) {
        if (list[i].id === id) {
            return i;
        }
    }

    return -1;
}
function getIndexName(list, name) {
    for (var i = 0; i < list.length; i++ ) {
        if (list[i].name === name) {
            return true;
        }
    }
    return false;
}

var foodApi = Vue.resource('/foodlist');

Vue.component('food-form', {
    props: ['foods', 'foodAttr', 'message', 'setMessage'],
    data: function() {
        return {
            kitchenReq: '',
            inStock: '',
            name: '',
            id: ''
        }
    },
    watch: {
        foodAttr: function(newVal, oldVal) {
            this.name = newVal.name;
            this.inStock = newVal.inStock;
            this.kitchenReq = newVal.kitchenReq;
            this.id = newVal.id;
        }
    },
    template:
        '<div>' +
            '<div v-if="message.code === true" class="alert alert-success" >' +
                '{{message.message}}' +
            '</div>' +
            '<div v-if="message.code === false" class="alert alert-danger" >' +
                '{{message.message}}' +
            '</div>' +
            '<div class="form-inline col-md-6 mt-3 mb-3 ml-5" align="center">' +
                '<input class="form-control" type="text" readonly placeholder="Название" v-model="name">' +
                '<input class="form-control ml-1" type="number" placeholder="Сколько кг в наличии" v-model="inStock">' +
                '<input class="form-control" type="text" readonly hidden placeholder="Сколько кг нужно" v-model="kitchenReq">' +
                '<input class="btn btn-primary ml-2" type="button" value="Save" @click="save" />' +
            '</div>' +
        '</div>',
    methods: {
        save: function() {
            if (this.inStock !== "") {

                var food = {name: this.name, kitchenReq: this.kitchenReq, inStock: this.inStock};

                if (this.id) {
                    food.id = this.id;
                    foodApi.update({}, food).then(result =>
                        result.json().then(data => {
                            if(data.message.code === true){
                                var index = getIndex(this.foods, data.body.id);
                                this.foods.splice(index, 1, data.body);
                            }
                            this.setMessage(data.message);
                            if(data.message.code){
                                this.id = '';
                                this.name = '';
                                this.inStock = '';
                                this.kitchenReq = '';
                            }

                        })
                    )
                } else {
                    this.setMessage({code: false, message:"Выберите продукт, который хотите изменить"})
                }
            } else {
                this.setMessage({code: false, message:"Колличество в наличии должно быть выражено числом"});
                this.inStock = ' ';
                this.name = '';
            }
        }
    }
});

Vue.component('food-row', {
    props: ['food', 'editMethod', 'foods', 'activeUser'],
    template:
            '<div>' +
        '           <td height="40" width="250px">{{ food.name }}</td>' +
        '           <td width="150px">{{ food.inStock }}</td>' +
        '           <td width="150px">{{ food.kitchenReq }}</td>' +
        '           <td v-if="activeUser.roles.indexOf(\'STOCK\') !== -1" align="center" width="200px"><input class="btn btn-primary " type="button" value="Edit" @click="edit"/></td>' +
            '</div>',
    methods: {
        edit: function() {
            this.editMethod(this.food);
        }
    }
});

Vue.component('foods-list', {
    props: ['foods', 'activeUser', 'message', 'setMessage'],
    data: function() {
        return {
            food: null
        }
    },
    template:
        '<div align="center">' +
            '<h1 v-if="activeUser.role == \'STOCK\'" class="mt-3">Склад</h1>' +
            '<h1 v-else class="mb-4">Склад</h1>' +
            '<food-form v-if="activeUser.roles.indexOf(\'STOCK\') !== -1" :setMessage="setMessage" :message="message" :foods="foods" :foodAttr="food" />' +
            '<table-header :activeUser="activeUser"/>' +
            '<table class="table-bordered table-light " cellpadding="7">' +
                '<tr align="center"><food-row v-for="food in foods" :key="food.id"' +
                                                ' :food="food"' +
                                                ' :editMethod="editMethod"' +
                                                ' :foods="foods"' +
                                                ' :activeUser="activeUser"/></tr>' +
            '</table>' +
        '</div>',
    methods: {
        editMethod: function(food) {
            this.food = food;
        }
    }
});

Vue.component('table-header', {
    props:['activeUser'],
    template:
        '<table class="table-bordered table-dark mt-2 " cellpadding="10">' +
        '    <tr align="center">' +
        '      <th scope="col" width="250px" height="55">Название</th>' +
        '      <th scope="col" width="150px">В наличии,кг</th>' +
        '      <th scope="col" width="150px">Запрос,кг</th>' +
        '      <th v-if="activeUser.roles.indexOf(\'STOCK\') !== -1" scope="col" width="200px">Изменить</th>' +
        '    </tr>' +
        '</table>'
});

var app = new Vue({
    el: '#app',
    template: '<foods-list :setMessage="setMessage" :message="message" :foods="foods" :activeUser="activeUser" />',
    data: {
        foods: [],
        activeUser: {},
        message: {}
    },
    methods: {
        getFoods: function () {
            this.$http.get('/foodlist').then(function (response){
                this.foods = response.data
            }, function (error) {
            })
        },
        getActiveUser: function () {
            this.$http.get('/activeuser').then(function (response){
                this.activeUser = response.data
            }, function (error) {
            })
        },
        setMessage: function (message) {
            this.message = message;
        },
        getMessage: function () {
            this.message = {code:"", message: ""}
        }
    },
    created: function () {
        this.getFoods();
        this.getActiveUser();
        this.getMessage();
    }
});