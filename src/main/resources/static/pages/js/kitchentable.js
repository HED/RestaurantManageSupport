function getIndex(list, id) {
    for (var i = 0; i < list.length; i++ ) {
        if (list[i].id === id) {
            return i;
        }
    }
    return -1;
}
function getIndexName(list, name) {
    for (var i = 0; i < list.length; i++ ) {
        if (list[i].name === name) {
            return true;
        }
    }
    return false;
}

var foodApi = Vue.resource('/kitchen');

Vue.component('food-form', {
    props: ['foods', 'foodAttr', 'message', 'setMessage'],
    data: function() {
        return {
            kitchenReq: '',
            name: '',
            id: ''
        }
    },
    watch: {
        foodAttr: function(newVal, oldVal) {
            this.name = newVal.name;
            this.kitchenReq = newVal.kitchenReq;
            this.id = newVal.id;
        }
    },
    template:
        '<div> ' +
            '<div v-if="message.code === true" class="alert alert-success" >' +
                '{{message.message}}' +
            '</div>' +
            '<div v-if="message.code === false" class="alert alert-danger" >' +
                '{{message.message}}' +
            '</div>' +
            '<div class="form-inline col-md-6 mt-3 mb-3 ml-5" align="center">' +
                '<input class="form-control" type="text" placeholder="Название" v-model="name">' +
                '<input class="form-control ml-1" type="number" placeholder="Сколько кг нужно" v-model="kitchenReq">' +
                '<input class="btn btn-primary ml-2" type="button" value="Save" @click="save" />' +
            '</div>' +
        '</div>',
    methods: {
        save: function() {
            if(this.kitchenReq !== ""){
                var food = {name: this.name, kitchenReq: this.kitchenReq};

                if (this.id) {
                    food.id = this.id;
                    foodApi.update({}, food).then(result =>
                        result.json().then(data => {
                            if(data.message.code){
                                var index = getIndex(this.foods, data.body.id);
                                this.foods.splice(index, 1, data.body);
                            }
                            this.setMessage(data.message);
                            if(data.message.code){
                                this.id = '';
                                this.name = '';
                                this.kitchenReq = '';
                            }
                        })
                    )
                } else {
                    if (getIndexName(this.foods, this.name)) {
                        console.log("can't add, becouse this name already exists");
                        this.setMessage({code: false, message:"Продукт с таким именем " + this.name + " уже существует"});
                        this.kitchenReq = '';
                        this.name = '';
                    } else {
                        foodApi.save({}, food).then(result =>
                            result.json().then(data => {
                                if(data.message.code === true){this.foods.push(data.body);}
                                this.kitchenReq = '';
                                this.name = '';
                                this.setMessage(data.message);
                            })
                        )
                    }
                }
            }else {
                this.setMessage({code:false, message:"Колличество запроса должно быть выражено числом"});
                // this.kitchenReq = ' ';
                // this.name = '';
            }
        }
    }
});

Vue.component('food-row', {
    props: ['food', 'editMethod', 'foods', 'activeUser', 'setMessage' ],
    template:
        '<div>' +
            '<tr align="center">' +
                '<td width="350px" height="40">{{ food.name }}</td>' +
                '<td width="150px">{{ food.kitchenReq }}</td>' +
                '<td v-if="activeUser.roles.indexOf(\'KITCHEN\') !== -1" width="200px">' +
                    '<span align="center">' +
                        '<input class="btn btn-primary " type="button" value="Edit" @click="edit" />' +
                        '<input class="btn btn-primary ml-1 " type="button" value="X" @click="del" />' +
                    '</span>' +
                '</td>' +
            '</tr>' +
        '</div>',
    methods: {
        edit: function() {
            this.editMethod(this.food);
        },
        del: function() {
            foodApi.delete({},this.food).then(result =>
                result.json().then(data => {
                    if(data.message.code === true){
                        this.foods.splice(this.foods.indexOf(this.food), 1)
                    }
                    this.setMessage(data.message)
                })
            )
        }
    }
});

Vue.component('foods-list', {
    props: ['foods','activeUser', 'message', 'setMessage'],
    data: function() {
        return {
            food: null
        }
    },
    template:
        '<div align="center">' +
            '<h1 class="mt-3">Кухня</h1>' +
            '<food-form v-if="activeUser.roles.indexOf(\'KITCHEN\') !== -1" :setMessage="setMessage" :message="message" :foods="foods" :foodAttr="food" />' +
            '<table-header :activeUser="activeUser" />' +
            '<table class="table-bordered table-light" cellpadding="7" >' +
                '<food-row v-for="food in foods" :setMessage="setMessage" :key="food.id"' +
                                        ' :food="food"' +
                                        ' :editMethod="editMethod"' +
                                        ' :foods="foods"' +
                                        ' :activeUser="activeUser" />' +
            '</table>' +
        '</div>',
    methods: {
        editMethod: function(food) {
            this.food = food;
        }
    }
});

Vue.component('table-header', {
    props:['activeUser'],
    template:
        '<div>' +
        '<table class="table-bordered table-dark mt-2 " cellpadding="10" >' +
        '   <tr align="center">' +
                '<th scope="col" width="350px" height="55">Название</th>' +
                '<th scope="col" width="150px">Запрос,кг</th>' +
                '<th v-if="activeUser.roles.indexOf(\'KITCHEN\') !== -1 " scope="col" width="200px">Изменить</th>' +
            '</tr>' +
        '</table>' +
        '</div>'
});


var app = new Vue({
    el: '#app',
    template: '<foods-list :setMessage="setMessage" :message="message" :foods="foods" :activeUser="activeUser" />',
    data: {
        foods: [],
        activeUser: {},
        message: {}
    },
    methods: {
        getFoods: function () {
            this.$http.get('/kitchen').then(function (response){
                this.foods = response.data
            }, function (error) {
            })
        },
        getActiveUser: function () {
            this.$http.get('/activeuser').then(function (response){
                this.activeUser = response.data
            }, function (error) {
            })
        },
        setMessage: function (message) {
            this.message = message;
        },
        getMessage: function () {
            this.message = {code:"", message: ""}
        }
    },
    created: function () {
        this.getFoods();
        this.getActiveUser();
        this.getMessage();
    }
});