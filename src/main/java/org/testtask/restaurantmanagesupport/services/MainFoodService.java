package org.testtask.restaurantmanagesupport.services;

import org.testtask.restaurantmanagesupport.dto.MainFoodDTO;
import org.testtask.restaurantmanagesupport.dto.Response;
import org.testtask.restaurantmanagesupport.entitys.MainFood;

import javax.transaction.Transactional;
import java.util.List;

public interface MainFoodService {

    MainFood findByName(String name);

    List<MainFood> getAll();

    Response<MainFood> edit(MainFoodDTO mainFoodDTO);

    Response<MainFood> del(String name);

}
