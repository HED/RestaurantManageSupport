package org.testtask.restaurantmanagesupport.services;

import org.testtask.restaurantmanagesupport.dto.AnswerMsg;
import org.testtask.restaurantmanagesupport.dto.KitchenReqDTO;
import org.testtask.restaurantmanagesupport.dto.Response;
import org.testtask.restaurantmanagesupport.entitys.KitchenReq;

import javax.transaction.Transactional;
import java.util.List;

public interface KitchenReqService {

    List<KitchenReq> getAll();

    Response<KitchenReq> edit(KitchenReqDTO kitchenReqDTO);

    @Transactional
    Response<KitchenReq> add(KitchenReqDTO kitchenReqDTO);

    Response<KitchenReq> del(Long id, String name);

}
