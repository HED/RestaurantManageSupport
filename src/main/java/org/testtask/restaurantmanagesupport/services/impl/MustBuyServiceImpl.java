package org.testtask.restaurantmanagesupport.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dao.MustBuyDAO;
import org.testtask.restaurantmanagesupport.dto.MustBuyDTO;
import org.testtask.restaurantmanagesupport.entitys.MustBuy;
import org.testtask.restaurantmanagesupport.mappers.MustBuyMapper;
import org.testtask.restaurantmanagesupport.services.MustBuyService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MustBuyServiceImpl implements MustBuyService {

    private MustBuyDAO mustBuyDao;
    private MustBuyMapper mustBuyMapper;
    Logger logger = LoggerFactory.getLogger(MustBuyServiceImpl.class);

    @Autowired
    public MustBuyServiceImpl(MustBuyDAO mustBuyDao, MustBuyMapper mustBuyMapper) {
        this.mustBuyDao = mustBuyDao;
        this.mustBuyMapper = mustBuyMapper;
    }


    @Override
    public List<MustBuy> findAll() {
        return mustBuyDao.findAll();
    }

    @Transactional
    @Override
    public MustBuy edit(MustBuyDTO mustBuyDTO) {
        String name = mustBuyDTO.getName();
        try {
            Long id = mustBuyDao.findByName(name).getId();
            mustBuyDTO.setId(id);
            logger.warn("Продукт '" + name +"'  найден в списке закупки и будет изменён");
        }catch (NullPointerException e){
            logger.warn("Продукт '" + name + "' не найден в списке закупки и будет добавлен");
        }
        return mustBuyDao.save(mustBuyMapper.convertToEntity(mustBuyDTO));
    }

    @Transactional
    @Override
    public void delByName(String name) {
        try {
            Long id = mustBuyDao.findByName(name).getId();
            mustBuyDao.deleteById(id);
            logger.info("Продукт '" + name + "' удалён из списка закупки");
        }catch (Exception e){
            logger.error("Продукт '" + name + "'  не найден и не удалён из списка закупки");
        }
    }
}
