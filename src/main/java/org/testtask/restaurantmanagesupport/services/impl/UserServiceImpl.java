package org.testtask.restaurantmanagesupport.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dao.UserDAO;
import org.testtask.restaurantmanagesupport.dto.AnswerMsg;
import org.testtask.restaurantmanagesupport.dto.Response;
import org.testtask.restaurantmanagesupport.dto.UserDTO;
import org.testtask.restaurantmanagesupport.entitys.Role;
import org.testtask.restaurantmanagesupport.entitys.User;
import org.testtask.restaurantmanagesupport.mappers.UserMapper;
import org.testtask.restaurantmanagesupport.services.UserService;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.testtask.restaurantmanagesupport.entitys.Role.ADMIN;
import static org.testtask.restaurantmanagesupport.entitys.Role.AGENT;

@Service
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;
    private UserMapper userMapper;
    private PasswordEncoder passwordEncoder;
    private AnswerMsg answerMsg;
    private User userBody;

    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    public UserServiceImpl(UserDAO userDAO, UserMapper userMapper, PasswordEncoder passwordEncoder) {
        this.userDAO = userDAO;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> findAll(){
        return userDAO.findAll();
    }

    @Override
    public Response<User> addUser(UserDTO userDTO){
        if(userDTO.getOriginpass().equals("") || userDTO.getUsername().equals("")){
            answerMsg = new AnswerMsg(false,"Пользователь не может быть добавлен без имени/пароля!");
            return new Response<User>(answerMsg,userBody);
        }
        if(userDAO.findByUsername(userDTO.getUsername()) != null){
            logger.error("Попытка добавить существующего пользователя!");
            answerMsg = new AnswerMsg(false,"Пользователь с логином '" + userDTO.getUsername() + "' уже есть!");
            return new Response<User>(answerMsg);
        }

        if(userDTO.getRole().equals("ALL")){

            Set<Role> roles = Arrays.stream(Role.values()).collect(Collectors.toSet());
            userDTO.setRoles(roles);

        }else {
            try {
                Role role = Role.valueOf(userDTO.getRole());
                userDTO.setRoles(Collections.singleton(role));
            } catch (NullPointerException e) {
                logger.error("Попытка добавить пользователя без роли или с некорректной ролью!");
                answerMsg = new AnswerMsg(false, "Пользователь '" + userDTO.getUsername() + "' не может быть без роли или с некорректной ролью!");
                return new Response<User>(answerMsg);
            }
        }





        userDTO.setPassword(passwordEncoder.encode(userDTO.getOriginpass()));

        userBody = userDAO.save(userMapper.convertToEntity(userDTO));
        answerMsg = new AnswerMsg(true,"Пользователь '" + userDTO.getUsername() + "' успешно добавлен!");
        return new Response<User>(answerMsg,userBody);
    }



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDAO.findByUsername(username);
    }

    @Override
    public Response<User> delById(UserDTO userDTO) {
        userDTO.setRoles(null);
        userDAO.deleteById(userDTO.getId());
        answerMsg = new AnswerMsg(true,"Пользователь '" + userDTO.getUsername() + "' успешно удалён!");
        return new Response<User>(answerMsg);
    }
    @Override
    public boolean findAdmin() {
        User admin = userDAO.findByRole("ADMIN");
        if(admin != null) {
            logger.error(admin.getUsername() + " " + admin.getOriginpass());
            return true;
        }
        return false;
    }
}
