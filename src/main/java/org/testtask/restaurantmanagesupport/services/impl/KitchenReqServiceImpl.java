package org.testtask.restaurantmanagesupport.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dao.KitchenReqDAO;
import org.testtask.restaurantmanagesupport.dto.AnswerMsg;
import org.testtask.restaurantmanagesupport.dto.KitchenReqDTO;
import org.testtask.restaurantmanagesupport.dto.MainFoodDTO;
import org.testtask.restaurantmanagesupport.dto.Response;
import org.testtask.restaurantmanagesupport.entitys.KitchenReq;
import org.testtask.restaurantmanagesupport.mappers.KitchenReqMapper;
import org.testtask.restaurantmanagesupport.services.KitchenReqService;
import org.testtask.restaurantmanagesupport.services.MainFoodService;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class KitchenReqServiceImpl implements KitchenReqService {

    private KitchenReqDAO kitchenReqDAO;
    private KitchenReqMapper kitchenReqMapper;
    private MainFoodService mainFoodService;
    private MainFoodDTO mainFoodDTO;
    private AnswerMsg answerMsg;
    private KitchenReq kitchenReqBody;

    Logger logger = LoggerFactory.getLogger(KitchenReqServiceImpl.class);

    @Autowired
    public KitchenReqServiceImpl(KitchenReqDAO kitchenReqDAO,
                                 KitchenReqMapper kitchenReqMapper,
                                 MainFoodServiceImpl mainFoodService
                                 ) {
        this.kitchenReqDAO = kitchenReqDAO;
        this.kitchenReqMapper = kitchenReqMapper;
        this.mainFoodService = mainFoodService;
    }

    @Override
    public List<KitchenReq> getAll() {
        return kitchenReqDAO.findAll();
    }

    @Transactional
    @Override
    public Response<KitchenReq> edit(KitchenReqDTO kitchenReqDTO) {
        if(kitchenReqDTO.getKitchenReq() <0){
            logger.error("Попытка назначить продукту с кухни отрицательное запросом");
            answerMsg = new AnswerMsg(false,"Нельзя назначать продукту отрицательное значение запроса");
            return new Response<KitchenReq>(answerMsg);
        }

        String newname = kitchenReqDTO.getName();
        String oldname = kitchenReqDAO.findById(kitchenReqDTO.getId()).orElseThrow(()->new EntityNotFoundException("404")).getName();
        if(!oldname.equals(newname)){
            logger.warn("Имя продукта '" + oldname + "' не изменено.");
            answerMsg = new AnswerMsg(false, "Имя продукта '" + oldname + "' не изменено. Имя продукта менять нельзя");
            return new Response<KitchenReq>(answerMsg);
        }

        Integer kitchenReq = kitchenReqDTO.getKitchenReq();
        Integer inStock = mainFoodService.findByName(newname).getInStock();
        Long id = mainFoodService.findByName(newname).getId();

        mainFoodDTO = new MainFoodDTO(id,newname,inStock,kitchenReq);
        logger.info("Параметр продукта '" + kitchenReqDTO.getName() + "'  kitchenReq изменён на" + kitchenReq);

        mainFoodService.edit(mainFoodDTO);
        logger.info("Продукт '" + kitchenReqDTO.getName() + "' изменён на странице склада");
        kitchenReqBody = kitchenReqDAO.save(kitchenReqMapper.convertToEntity(kitchenReqDTO));
        answerMsg = new AnswerMsg(true,"Продукт '" + kitchenReqDTO.getName() + "' изменён, значение запроса: " + kitchenReq);
        return new Response<KitchenReq>(answerMsg,kitchenReqBody);
    }

    @Transactional
    @Override
    public Response<KitchenReq> add(KitchenReqDTO kitchenReqDTO) {
        if(kitchenReqDTO.getKitchenReq() <0){
            logger.error("Попытка добавить новый продукт с кухни с отрицательным запросом");
            answerMsg = new AnswerMsg(false,"Нельзя добавлять продукт с отрицательным значением");
            return new Response<KitchenReq>(answerMsg);
        }
        logger.warn("Добавляется новый продукт: '" + kitchenReqDTO.getName() + "'");

        logger.warn("Продукт добавляется в список склада с параметрами" +
                " newname: '" + kitchenReqDTO.getName() +
                "' inStok: '0' " +
                " kitchenReq '" + kitchenReqDTO.getKitchenReq() + "'");
        mainFoodDTO = new MainFoodDTO(kitchenReqDTO.getName(),0,kitchenReqDTO.getKitchenReq());
        mainFoodService.edit(mainFoodDTO);
        logger.info("Продукт '" + kitchenReqDTO.getName() + "' добавлен на странице склада");

        kitchenReqBody = kitchenReqDAO.save(kitchenReqMapper.convertToEntity(kitchenReqDTO));
        answerMsg = new AnswerMsg(true,"Продукт '" + kitchenReqDTO.getName() + "' добавлен, значение запроса: " + kitchenReqDTO.getKitchenReq());
        return new Response<KitchenReq>(answerMsg,kitchenReqBody);
    }


    @Transactional
    @Override
    public Response<KitchenReq> del(Long id, String name) {
        try {
            mainFoodService.del(name);
            kitchenReqDAO.deleteById(id);
            logger.info("Продукт '" + name + "' удалён со страницы кухни");
            answerMsg = new AnswerMsg(true,"Продукт '" + name + "' удалён");
            return new Response<KitchenReq>(answerMsg);
        } catch (Exception e) {
            logger.error("Продукт '" + name + "' не удалён со страницы кухни " + " 404");
            answerMsg = new AnswerMsg(false,"Продукт '" + name + "' не удалён");
            return new Response<KitchenReq>(answerMsg);
        }
    }
}
