package org.testtask.restaurantmanagesupport.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dao.MainFoodDAO;
import org.testtask.restaurantmanagesupport.dto.AnswerMsg;
import org.testtask.restaurantmanagesupport.dto.MainFoodDTO;
import org.testtask.restaurantmanagesupport.dto.Response;
import org.testtask.restaurantmanagesupport.entitys.MainFood;
import org.testtask.restaurantmanagesupport.entitys.MustBuy;
import org.testtask.restaurantmanagesupport.mappers.MainFoodMapper;
import org.testtask.restaurantmanagesupport.mappers.MustBuyMapper;
import org.testtask.restaurantmanagesupport.services.MainFoodService;
import org.testtask.restaurantmanagesupport.services.MustBuyService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MainFoodServiceImpl implements MainFoodService {

    private MainFoodDAO mainFoodDAO;
    private MainFoodMapper mainFoodMapper;
    private MustBuyService mustBuyService;
    private MustBuyMapper mustBuyMapper;
    private AnswerMsg answerMsg;
    private MainFood mainFoodBody;

    Logger logger = LoggerFactory.getLogger(MainFoodServiceImpl.class);

    @Autowired
    public MainFoodServiceImpl(MainFoodDAO mainFoodDAO,
                               MainFoodMapper mainFoodMapper,
                               MustBuyServiceImpl mustBuyServiceImpl,
                               MustBuyMapper mustBuyMapper
    ){
        this.mainFoodDAO = mainFoodDAO;
        this.mainFoodMapper = mainFoodMapper;
        this.mustBuyService = mustBuyServiceImpl;
        this.mustBuyMapper = mustBuyMapper;
    }

    @Override
    public MainFood findByName(String name) {
       return mainFoodDAO.findByName(name);
    }

    @Override
    public List<MainFood> getAll() {
        return mainFoodDAO.findAll();
    }

    @Transactional
    @Override
    public Response<MainFood> edit(MainFoodDTO mainFoodDTO) {
        if(mainFoodDTO.getInStock() <0){
            logger.error("Попытка изменить продукт '" + mainFoodDTO.getName() + "' со страницы склада с отрицательным колличеством в наличии");
            answerMsg = new AnswerMsg(false,"Нельзя задавать продукту отрицательное значение");
            return new Response<MainFood>(answerMsg);
        }

        String name = mainFoodDTO.getName();
        Integer mustBuy = mainFoodDTO.getKitchenReq() - mainFoodDTO.getInStock();
        logger.info("Разница (заказ с кухни - кол-во на складе):  " + mustBuy.toString() + " + 2");
        mustBuy += 2;
        if(mustBuy >= 1) {
            MustBuy mustBuyList = new MustBuy(name, mustBuy);
            mustBuyService.edit(mustBuyMapper.convertToDTO(mustBuyList));
            logger.info("На страницу закупуки добавлен продукт: '" + name + "' с коллчиеством: '" + mustBuy + "'");
        }else {
            logger.info("Разница (заказ с кухни - кол-во на складе):  " + mustBuy.toString());
            mustBuyService.delByName(name);
        }

        mainFoodBody = mainFoodDAO.save(mainFoodMapper.convertToEntity(mainFoodDTO));
        logger.info("на страницу склада добавлен продукт " + name);

        answerMsg = new AnswerMsg(true,"Продукт '" + mainFoodDTO.getName() + "' изменён, в наличии: " + mainFoodDTO.getInStock());
        return new Response<MainFood>(answerMsg,mainFoodBody);
    }

    @Transactional
    @Override
    public Response<MainFood> del(String name) {
        Long id = mainFoodDAO.findByName(name).getId();
        try {
            mainFoodDAO.deleteById(id);
            mustBuyService.delByName(name);
            logger.info("Продукт '" + name + "' удалён со страниц склада");
            answerMsg = new AnswerMsg(true,"Продукт '" + name + "' удалён");
            return new Response<MainFood>(answerMsg);
        } catch (Exception e) {
            logger.error("Продукт '" + name + "' не удалён со страниц склада");
            answerMsg = new AnswerMsg(false,"Продукт '" + name + "' не удалён");
            return new Response<MainFood>(answerMsg);
        }

    }


}
