package org.testtask.restaurantmanagesupport.services;


import org.testtask.restaurantmanagesupport.dto.MustBuyDTO;
import org.testtask.restaurantmanagesupport.entitys.MustBuy;

import java.util.List;

public interface MustBuyService {

    List<MustBuy> findAll();

    MustBuy edit(MustBuyDTO mustBuyDTO);


    void delByName(String name);
}
