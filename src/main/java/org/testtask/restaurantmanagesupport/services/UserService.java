package org.testtask.restaurantmanagesupport.services;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.testtask.restaurantmanagesupport.dto.Response;
import org.testtask.restaurantmanagesupport.dto.UserDTO;
import org.testtask.restaurantmanagesupport.entitys.User;

import java.util.List;

public interface UserService extends UserDetailsService {


    List<User> findAll();

    Response<User> addUser(UserDTO userDTO);

    Response<User> delById(UserDTO userDTO);

    boolean findAdmin();
}
