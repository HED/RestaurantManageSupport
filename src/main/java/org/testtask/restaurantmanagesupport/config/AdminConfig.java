package org.testtask.restaurantmanagesupport.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.testtask.restaurantmanagesupport.dto.UserDTO;
import org.testtask.restaurantmanagesupport.services.impl.UserServiceImpl;

@Configuration
public class AdminConfig {
    @Value("${default.admin.username}")
    String defaultAdminUsername;
    @Value("${default.admin.password}")
    String defaultAdminPass;



    @Bean
    public CommandLineRunner addAdmin(UserServiceImpl userService){
        return args -> {
            if(!userService.findAdmin()) {
                userService.addUser(new UserDTO(defaultAdminUsername, defaultAdminPass, "ALL"));
            }
        };
    }
}
