package org.testtask.restaurantmanagesupport.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.testtask.restaurantmanagesupport.dto.Response;
import org.testtask.restaurantmanagesupport.dto.UserDTO;
import org.testtask.restaurantmanagesupport.entitys.User;
import org.testtask.restaurantmanagesupport.mappers.UserMapper;
import org.testtask.restaurantmanagesupport.services.UserService;
import org.testtask.restaurantmanagesupport.services.impl.UserServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/users")
@PreAuthorize("hasAuthority('ADMIN')")
public class UserController {

    UserService userService;
    UserMapper userMapper;

    @Autowired
    public UserController(UserServiceImpl userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    Logger logger = LoggerFactory.getLogger(MainFoodController.class);

    @GetMapping
    public List<User> getUsers(){
        logger.info("Вывод списка пользователей");
        return userService.findAll();
    }

    @PostMapping
    public Response<User> addUser(@RequestBody UserDTO newUser){
        logger.info("Добавление пользователя " + newUser.getUsername() + " c ролью " + newUser.getRole());
        return userService.addUser(newUser);
    }

    @DeleteMapping
    public Response<User> delUser(@RequestBody UserDTO delUser){
        logger.info("Удаление пользователя " + delUser.getUsername());
        return userService.delById(delUser);
    }


}
