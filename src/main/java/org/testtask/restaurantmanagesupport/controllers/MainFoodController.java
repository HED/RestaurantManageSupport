package org.testtask.restaurantmanagesupport.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.testtask.restaurantmanagesupport.dto.MainFoodDTO;
import org.testtask.restaurantmanagesupport.dto.Response;
import org.testtask.restaurantmanagesupport.entitys.MainFood;
import org.testtask.restaurantmanagesupport.mappers.MainFoodMapper;
import org.testtask.restaurantmanagesupport.services.MainFoodService;
import org.testtask.restaurantmanagesupport.services.impl.MainFoodServiceImpl;

import java.util.List;


@RestController
@RequestMapping("/foodlist")
public class MainFoodController {

    private MainFoodService service;
    private MainFoodMapper mapper;

    @Autowired
    public MainFoodController(MainFoodServiceImpl service, MainFoodMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    Logger logger = LoggerFactory.getLogger(MainFoodController.class);

    @GetMapping
    public List<MainFood> getAllFood(){
        logger.info("Вывод списка продуктов на страницу склада");
        return service.getAll();
    }


    @PutMapping
    @PreAuthorize("hasAuthority('STOCK')")
    public Response<MainFood> editFood(@RequestBody MainFoodDTO editMainFoodList) {
        logger.info("Изменение продукта " + editMainFoodList.getName() + " со страницы склада");
        return service.edit(editMainFoodList);
    }


}
