package org.testtask.restaurantmanagesupport.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.testtask.restaurantmanagesupport.entitys.MustBuy;
import org.testtask.restaurantmanagesupport.services.MustBuyService;
import org.testtask.restaurantmanagesupport.services.impl.MustBuyServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/mustbuylist")
public class MustBuyController {

    private MustBuyService service;

    @Autowired
    public MustBuyController(MustBuyServiceImpl service) {
        this.service = service;
    }

    Logger logger = LoggerFactory.getLogger(MainFoodController.class);

    @GetMapping
    public List<MustBuy> getAll(){
        logger.info("Вывод списка продуктов на страницу закупки ");
        return service.findAll();
    }

}
