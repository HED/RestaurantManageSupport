package org.testtask.restaurantmanagesupport.controllers;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.testtask.restaurantmanagesupport.entitys.User;

@RestController
@RequestMapping("/activeuser")
public class ShowActiveUser {
    @GetMapping
    public User getUser(@AuthenticationPrincipal User user) {
        return user;
    }
}
