package org.testtask.restaurantmanagesupport.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.testtask.restaurantmanagesupport.dto.AnswerMsg;
import org.testtask.restaurantmanagesupport.dto.KitchenReqDTO;
import org.testtask.restaurantmanagesupport.dto.Response;
import org.testtask.restaurantmanagesupport.entitys.KitchenReq;
import org.testtask.restaurantmanagesupport.mappers.KitchenReqMapper;
import org.testtask.restaurantmanagesupport.services.KitchenReqService;
import org.testtask.restaurantmanagesupport.services.impl.KitchenReqServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/kitchen")
public class KitchenReqController {

    private KitchenReqService service;
    private KitchenReqMapper mapper;

    @Autowired
    public KitchenReqController(KitchenReqServiceImpl service, KitchenReqMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    Logger logger = LoggerFactory.getLogger(MainFoodController.class);

    @GetMapping
    public List<KitchenReq> getAllFood(){
        logger.info("Вывод списка продуктов на страницу кухни");
        return service.getAll();
    }

    @PostMapping
    @PreAuthorize("hasAuthority('KITCHEN')")
    public Response<KitchenReq> addFood(@RequestBody KitchenReqDTO newKitchenReq){
        logger.info("Добавление продукта " + newKitchenReq.getName()+" со страницы кухни");
        return service.add(newKitchenReq);
    }


    @PutMapping
    @PreAuthorize("hasAuthority('KITCHEN')")
    public Response<KitchenReq> editFood(@RequestBody KitchenReqDTO editKitchenReq) {
        logger.info("Изменение продукта" + editKitchenReq.getName() +" со страницы кухни");
        return service.edit(editKitchenReq);
    }


    @DeleteMapping()
    @PreAuthorize("hasAuthority('KITCHEN')")
    public Response<KitchenReq> delete(@RequestBody KitchenReqDTO delKitchenReq) {
        logger.info("Удалени  продукта " + delKitchenReq.getName() + " со страницы кухни");
        return service.del(delKitchenReq.getId(), delKitchenReq.getName());
    }

}
