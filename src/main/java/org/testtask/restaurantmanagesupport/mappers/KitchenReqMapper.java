package org.testtask.restaurantmanagesupport.mappers;

import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dto.KitchenReqDTO;
import org.testtask.restaurantmanagesupport.entitys.KitchenReq;

@Service
public class KitchenReqMapper {

    public KitchenReqDTO convertToDTO(KitchenReq kitchenReq) {
        return new KitchenReqDTO(kitchenReq.getId(),
                kitchenReq.getName(),
                kitchenReq.getKitchenReq());
    }

    public KitchenReq convertToEntity(KitchenReqDTO kitchenReqDTO) {
        return new KitchenReq(kitchenReqDTO.getId(),
                kitchenReqDTO.getName(),
                kitchenReqDTO.getKitchenReq());
    }

}
