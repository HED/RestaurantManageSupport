package org.testtask.restaurantmanagesupport.mappers;

import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dto.MustBuyDTO;
import org.testtask.restaurantmanagesupport.entitys.MustBuy;

@Service
public class MustBuyMapper {

    public MustBuyDTO convertToDTO(MustBuy mustBuy) {
        return new MustBuyDTO(mustBuy.getId(),
                mustBuy.getName(),
                mustBuy.getMustBuy());
    }

    public MustBuy convertToEntity(MustBuyDTO foodstuffDTO) {
        return new MustBuy(foodstuffDTO.getId(),
                foodstuffDTO.getName(),
                foodstuffDTO.getMustBuy());
    }

}
