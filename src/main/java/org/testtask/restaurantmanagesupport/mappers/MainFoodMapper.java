package org.testtask.restaurantmanagesupport.mappers;

import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dto.MainFoodDTO;
import org.testtask.restaurantmanagesupport.entitys.MainFood;

@Service
public class MainFoodMapper {

    public MainFoodDTO convertToDTO(MainFood mainFood){
        return new MainFoodDTO(mainFood.getId(),
                mainFood.getName(),
                mainFood.getInStock(),
                mainFood.getKitchenReq());
    }

    public MainFood convertToEntity(MainFoodDTO mainFoodDTO){
        return new MainFood(mainFoodDTO.getId(),
                mainFoodDTO.getName(),
                mainFoodDTO.getInStock(),
                mainFoodDTO.getKitchenReq());
    }
}
