package org.testtask.restaurantmanagesupport.mappers;

import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dto.UserDTO;
import org.testtask.restaurantmanagesupport.entitys.User;

@Service
public class UserMapper {

    public UserDTO convertToDTO(User user) {
        return new UserDTO(user.getId(),
                user.getUsername(),
                user.getOriginpass(),
                user.getPassword(),
                user.getRole(),
                user.getRoles());
    }

    public User convertToEntity(UserDTO userDTO) {
        return new User(userDTO.getId(),
                userDTO.getUsername(),
                userDTO.getOriginpass(),
                userDTO.getPassword(),
                userDTO.getRole(),
                userDTO.getRoles());
    }

}
