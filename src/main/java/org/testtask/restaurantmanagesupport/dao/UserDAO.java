package org.testtask.restaurantmanagesupport.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.testtask.restaurantmanagesupport.entitys.User;

@Repository
public interface UserDAO extends JpaRepository<User,Long> {

    User findByUsername(String username);

    User findByRole(String role);
}
