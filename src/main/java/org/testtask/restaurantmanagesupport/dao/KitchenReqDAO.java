package org.testtask.restaurantmanagesupport.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.testtask.restaurantmanagesupport.entitys.KitchenReq;

import java.util.Optional;

@Repository
public interface KitchenReqDAO extends JpaRepository<KitchenReq,Long> {


}
