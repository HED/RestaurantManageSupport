package org.testtask.restaurantmanagesupport.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.testtask.restaurantmanagesupport.entitys.MainFood;

@Repository
public interface MainFoodDAO extends JpaRepository<MainFood,Long> {
    void deleteById(Long id);
    MainFood findByName(String name);
}
