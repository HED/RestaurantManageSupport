package org.testtask.restaurantmanagesupport.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.testtask.restaurantmanagesupport.entitys.MustBuy;

@Repository
public interface MustBuyDAO extends JpaRepository<MustBuy,Long> {

    MustBuy findByMustBuy(Integer mustBuy);
    MustBuy findByName(String name);

}
