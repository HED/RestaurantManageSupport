package org.testtask.restaurantmanagesupport.entitys;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MustBuy {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private Integer mustBuy;

    MustBuy(){}

    public MustBuy(String name, Integer mustBuy) {
        this.name = name;
        this.mustBuy = mustBuy;
    }

    public MustBuy(Long id, String name, Integer mustBuy) {
        this.id = id;
        this.name = name;
        this.mustBuy = mustBuy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMustBuy() {
        return mustBuy;
    }

    public void setMustBuy(Integer mustBuy) {
        this.mustBuy = mustBuy;
    }
}
