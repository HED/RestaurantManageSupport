package org.testtask.restaurantmanagesupport.entitys;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    STOCK, KITCHEN, AGENT, ADMIN;

    @Override
    public String getAuthority() {
        return name();
    }
}
