package org.testtask.restaurantmanagesupport.entitys;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class KitchenReq {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private Integer kitchenReq;

    public KitchenReq(){}

    public KitchenReq(String name, Integer kitchenReq) {
        this.name = name;
        this.kitchenReq = kitchenReq;
    }

    public KitchenReq(Long id, String name, Integer kitchenReq) {
        this.id = id;
        this.name = name;
        this.kitchenReq = kitchenReq;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getKitchenReq() {
        return kitchenReq;
    }

    public void setKitchenReq(Integer kitchenReq) {
        this.kitchenReq = kitchenReq;
    }
}
