package org.testtask.restaurantmanagesupport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestaurantManageSupportApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestaurantManageSupportApplication.class, args);
		System.out.println("===========================================================READY===================================================================");
	}

}
