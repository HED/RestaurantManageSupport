package org.testtask.restaurantmanagesupport.dto;

import org.testtask.restaurantmanagesupport.entitys.Role;

import java.util.Set;

public class UserDTO {

    private Long id;

    private String username;
    private String originpass;
    private String password;
    private String role;

    private Set<Role> roles;

    public UserDTO(){}

    public UserDTO(Long id, String username, String originpass, String password, String role, Set<Role> roles) {
        this.id = id;
        this.username = username;
        this.originpass = originpass;
        this.password = password;
        this.role = role;
        this.roles = roles;
    }

    public UserDTO(String username, String originpass, String role) {
        this.username = username;
        this.originpass = originpass;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getOriginpass() {
        return originpass;
    }

    public void setOriginpass(String originpass) {
        this.originpass = originpass;
    }
}
