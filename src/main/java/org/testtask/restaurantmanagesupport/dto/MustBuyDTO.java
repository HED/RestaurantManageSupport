package org.testtask.restaurantmanagesupport.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class MustBuyDTO  {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private Integer mustBuy;


    public MustBuyDTO(String name, Integer mustBuy) {
        this.name = name;
        this.mustBuy = mustBuy;
    }

    public MustBuyDTO(Long id, String name, Integer mustBuy) {
        this.id = id;
        this.name = name;
        this.mustBuy = mustBuy;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMustBuy() {
        return mustBuy;
    }

    public void setMustBuy(Integer mustBuy) {
        this.mustBuy = mustBuy;
    }
}
