package org.testtask.restaurantmanagesupport.dto;

public class KitchenReqDTO  {

    private Long id;

    private String name;
    private Integer kitchenReq;

    public KitchenReqDTO(){}

    public KitchenReqDTO(String name, Integer kitchenReq) {
        this.name = name;
        this.kitchenReq = kitchenReq;
    }

    public KitchenReqDTO(Long id, String name, Integer kitchenReq) {
        this.id = id;
        this.name = name;
        this.kitchenReq = kitchenReq;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getKitchenReq() {
        return kitchenReq;
    }

    public void setKitchenReq(Integer kitchenReq) {
        this.kitchenReq = kitchenReq;
    }

}
