package org.testtask.restaurantmanagesupport.dto;

public class Response<T> {

    private AnswerMsg message;
    private T body;

    public Response(AnswerMsg message, T body) {
        this.message = message;
        this.body = body;
    }

    public Response(AnswerMsg message) {
        this.message = message;
    }

    public AnswerMsg getMessage() {
        return message;
    }

    public void setMessage(AnswerMsg message) {
        this.message = message;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
