package org.testtask.restaurantmanagesupport.dto;

public class MainFoodDTO {

    private Long id;

    private String name;
    private Integer inStock;
    private Integer kitchenReq;

    MainFoodDTO(){ }

    public MainFoodDTO(String name, Integer inStock, Integer kitchenReq) {
        this.name = name;
        this.inStock = inStock;
        this.kitchenReq = kitchenReq;
    }

    public MainFoodDTO(Long id, String name, Integer inStock, Integer kitchenReq ) {
        this.id = id;
        this.name = name;
        this.inStock = inStock;
        this.kitchenReq = kitchenReq;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public Integer getKitchenReq() {
        return kitchenReq;
    }

    public void setKitchenReq(Integer kitchenReq) {
        this.kitchenReq = kitchenReq;
    }

}
