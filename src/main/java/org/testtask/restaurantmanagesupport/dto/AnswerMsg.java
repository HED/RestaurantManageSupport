package org.testtask.restaurantmanagesupport.dto;

public class AnswerMsg {

    private boolean code;
    private String message;

    public AnswerMsg(boolean code, String message) {
        this.code = code;
        this.message = message;
    }

    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
