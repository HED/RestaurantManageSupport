package org.testtask.restaurantmanagesupport;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestaurantManageSupportApplicationTests {

	@Test
	public void contextLoads() {
	}

}
